Name:           perl-ExtUtils-ParseXS
Epoch:          2
Version:        3.51
Release:        2
Summary:        Convert Perl XS code into C code
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/pod/ExtUtils::ParseXS
Source0:        https://cpan.metacpan.org/authors/id/L/LE/LEONT/ExtUtils-ParseXS-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  gcc git coreutils make perl-devel perl-generators perl-interpreter perl(Config)
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76 perl(File::Spec) perl(strict) perl(warnings)
BuildRequires:  perl(Cwd) perl(Exporter) >= 5.57 perl(File::Basename) perl(re) perl(Symbol)
BuildRequires:  perl(attributes) perl(Carp) perl(DynaLoader) perl(ExtUtils::CBuilder)
BuildRequires:  perl(File::Temp) perl(lib) perl(overload) perl(Test::More) >= 0.47
Requires:       perl(Exporter) >= 5.57

%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}^perl\\(Exporter\\)$

%description
ExtUtils::ParseXS will compile XS code into C code by embedding the constructs necessary to let C functions manipulate Perl values
and creates the glue necessary to let Perl access those functions.
The compiler uses typemaps to determine how to map C function parameters and variables to Perl values.

%package_help

%prep
%autosetup -n ExtUtils-ParseXS-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 NO_PERLLOCAL=1
%make_build

%install
%make_install
%{_fixperms} $RPM_BUILD_ROOT/*
rm $RPM_BUILD_ROOT%{perl_vendorlib}/ExtUtils/xsubpp
ln -s ../../../../bin/xsubpp $RPM_BUILD_ROOT%{perl_vendorlib}/ExtUtils/

%check
make test

%files
%doc Changes
%{_bindir}/*
%{perl_vendorlib}/*

%files help
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 2:3.51-2
- drop useless perl(:MODULE_COMPAT) requirement

* Fri Jan 26 2024 dongyuzhen <dongyuzhen@h-partners.com> - 2:3.51-1
- upgrade vertion to 3.51:
  - fix OVERLOAD and FALLBACK handling
  - fix ExtUtils::ParseXS compatibility with perl < 5.8.8
  - add support for elifdef and elifndef
  - allow symbolic alias of default function
  - better support for duplicate ALIASes
  - make versions in ExtUtils-ParseXS consistent

* Wed Jul 19 2023 leeffo <liweiganga@uniontech.com> - 2:3.44-1
- upgrade to version 3.44

* Tue Oct 25 2022 dongyuzhen <dongyuzhen@h-partners.com> - 2:3.35-2
- Rebuild for next release

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 2:3.35-1
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: remove unnecessary files

* Sun Sep 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 1:3.39-419
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: revise changelog

* Sat Sep 14 2019 shidongdong <shidongdong5@huawei.com> - 1:3.39-418
- Package init
